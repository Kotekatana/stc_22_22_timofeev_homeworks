import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        HashMap<String,Integer> words = new HashMap<String,Integer>();

        String myString = "Hello Bye Hello Inno Bread Iron Bye Hello Bye Bye";
        String[] arrSting = myString.split(" ");
        for(int i = 0;i<arrSting.length;i++){
            if(words.containsKey(arrSting[i])){
                words.put(arrSting[i], words.get(arrSting[i])+1);
            }else{
                words.put(arrSting[i],1);
            }

        }
        System.out.println(words.entrySet().stream().max(Comparator.comparing(Map.Entry::getValue)));

    }
}