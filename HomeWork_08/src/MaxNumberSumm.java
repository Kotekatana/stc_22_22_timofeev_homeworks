public class MaxNumberSumm implements ArrayTask{
    @Override
    public int resolve(int[] array, int from, int to) {
        if(from>to){
            return -1;
        }
        int max = array[from];
        int summ = 0;
        for(int i = from;i<to;i++){
            if(array[i]>max){
                max = array[i];
            }
        }
        while (max!=0){
            summ+= max%10;
            max = max/10;
        }
        return summ;
    }
}
