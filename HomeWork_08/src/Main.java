public class Main {
    public static void main(String[] args) {
        CalcSumm summ = new CalcSumm();
        MaxNumberSumm maxSumm = new MaxNumberSumm();
        ArraysTasksResolver resolver = new ArraysTasksResolver();
        int[] array = {1,3,444,6,7,8,2,10};
        int resultSumm = resolver.resolveTask(array,summ,1,4);
        int resultMaxSumm = resolver.resolveTask(array,maxSumm,0,4);
        System.out.println(resultSumm);
        System.out.println(resultMaxSumm);
    }
}