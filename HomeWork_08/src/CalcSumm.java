public class CalcSumm implements ArrayTask{
    public int resolve(int[] array, int from, int to){
        int summ = 0;
        for(int i = from;i<to;i++){
            summ+=array[i];
        }
        return  summ;
    };
}
