import java.util.Random;

public class Main {

    public static void main(String[] args) {
        List<String> stringList = new ArrayList<>();

        stringList.add("Hello!");
        stringList.add("Bye!");
        stringList.add("Fine!");
        stringList.add("C++!");
        stringList.add("PHP!");
        stringList.add("Cobol!");

        List<String> stringList2 = new LinkedList<>();

        stringList2.add("1!");
        stringList2.add("2!");
        stringList2.add("3!");
        stringList2.add("4!");
        stringList2.add("5!");
        stringList2.add("6!");

        stringList.remove("PHP!");
        stringList.removeAt(0);

        stringList2.remove("1!");
        stringList2.removeAt(3);
    }
}