create table users(
    id serial primary key,
    first_name varchar(30) not null,
    last_name varchar(30),
    phone_number char(10) not null,
    experience integer,
    age integer check ( age > 0 and age <= 120),
    isDriver bool,
    rating real check ( rating > 0 and rating <=5 )
);
create table cars(
    id serial primary key,
    model varchar(30) not null,
    color varchar(15) not null,
    car_number char(9) not null,
    user_id serial references users(id)
);
create table orders (
  id serial primary key,
  user_id serial references users(id),
  car_id serial references cars(id),
  order_date date not null ,
  duration interval not null
);