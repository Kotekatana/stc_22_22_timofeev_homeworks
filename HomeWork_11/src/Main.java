public class Main {
    public static void main(String[] args) {
        CarsRepository carsRepository = new CarsRepositoryFileBasedImpl("input.txt");
        System.out.println(carsRepository.findAll());
        System.out.println(carsRepository.findAllWithColorOrMileage("Black",0));
        System.out.println(carsRepository.findUniqueModelsByPrice(700000,800000));
        System.out.println(carsRepository.findCarColorWithLowestPrice());
        System.out.println(carsRepository.carAvgPrice("Camry"));


    }
}