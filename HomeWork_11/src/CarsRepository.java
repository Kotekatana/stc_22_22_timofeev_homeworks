import java.util.List;

public interface CarsRepository {
    List<Car> findAll();
    List<String> findAllWithColorOrMileage(String color,Integer mileage);
    int findUniqueModelsByPrice(int from,int to);
    String findCarColorWithLowestPrice();
    Double carAvgPrice(String car);

}
