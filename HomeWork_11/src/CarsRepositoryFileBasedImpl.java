import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CarsRepositoryFileBasedImpl implements CarsRepository{
    private final String fileName;

    public CarsRepositoryFileBasedImpl(String fileName){
        this.fileName = fileName;
    }
    private static final Function<String,Car> stringToCarMapper = currentCar ->{
        String[] parts = currentCar.split("\\|");
        String number = parts[0];
        String model = parts[1];
        String color = parts[2];
        Integer mileAge = Integer.parseInt(parts[3]);
        Integer price = Integer.parseInt(parts[4]);
        return new Car(number,model,color,mileAge,price);
    };

    @Override
    public List<Car> findAll() {
        try {
            return new BufferedReader(new FileReader(fileName)).lines().map(stringToCarMapper).collect(Collectors.toList());
        }catch (IOException e ){
            System.err.println(e);
        }
        return null;
    }

    @Override
    public List<String> findAllWithColorOrMileage(String color,Integer mileage) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getColor().equals(color) || car.getMileage()<=mileage)
                    .map(Car::getNumber)
                    .collect(Collectors.toList());

        }catch (IOException e ){
            System.err.println(e);
        }
        return null;
    }

    @Override
    public int findUniqueModelsByPrice(int from, int to) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .distinct()
                    .filter(car -> car.getPrice()>from&& car.getPrice()<to)
                    .collect(Collectors.toList()).size();

        }catch (IOException e ){
            System.err.println(e);
        }
        return 0;
    }

    @Override
    public String findCarColorWithLowestPrice() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .min(Comparator.comparing(Car::getPrice))
                    .get().getColor();

        }catch (IOException e ){
            System.err.println(e);
        }
        return null;
    }

    @Override
    public Double carAvgPrice(String car) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .filter(Car-> Car.getModel().equals(car))
                    .mapToInt(Car::getPrice)
                    .average()
                    .getAsDouble();

        }catch (IOException e ){
            System.err.println(e);
        }
        return null;
    }
}
