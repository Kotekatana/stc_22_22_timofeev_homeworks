package com.kotekatana;

import com.kotekatana.repositories.UserRepositoryFileImpl;
import com.kotekatana.services.UserServiceImpl;
import com.kotekatana.validators.impl.EmailRegexValidator;
import com.kotekatana.validators.impl.EmailSimpleValidator;
import com.kotekatana.validators.impl.PasswordLengthValidator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        String regex = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        UserServiceImpl userService = context.getBean(UserServiceImpl.class);
        userService.signUp("kotekatana@rambler.ru","qwerty009");
    }
}