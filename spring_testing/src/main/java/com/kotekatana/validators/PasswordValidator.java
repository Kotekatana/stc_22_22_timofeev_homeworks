package com.kotekatana.validators;

public interface PasswordValidator {
        void validate(String password) throws IllegalArgumentException;
}
