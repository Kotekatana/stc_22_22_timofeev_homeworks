package com.kotekatana.validators.impl;

import com.kotekatana.validators.EmailValidator;

import java.util.regex.Pattern;

public class EmailRegexValidator implements EmailValidator {
    private String regex;

    public EmailRegexValidator(String regex) {
        this.regex = regex;
    }
    @Override
    public void validate(String email){
        if(!Pattern.compile(regex).matcher(email).find()){
            throw new IllegalArgumentException("Wrong email");
        }

    }
}
