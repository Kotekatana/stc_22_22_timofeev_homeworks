package com.kotekatana.validators.impl;

import com.kotekatana.validators.PasswordValidator;

public class PasswordLengthValidator implements PasswordValidator {
    private int minLength;
    public void setMinLength(int minLength) {
        this.minLength = minLength;
    }
    @Override
    public void validate(String password){
        if(password.length()<minLength){
            throw new IllegalStateException("Incorrect password");
        }
    }
}
