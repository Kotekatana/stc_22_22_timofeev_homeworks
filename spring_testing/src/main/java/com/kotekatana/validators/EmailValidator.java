package com.kotekatana.validators;

public interface EmailValidator {
    void validate(String email) throws IllegalArgumentException;
}
