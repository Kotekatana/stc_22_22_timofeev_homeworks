package com.kotekatana.services;

import com.kotekatana.models.User;
import com.kotekatana.repositories.UserRepository;
import com.kotekatana.repositories.UserRepositoryFileImpl;
import com.kotekatana.validators.EmailValidator;
import com.kotekatana.validators.PasswordValidator;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UserServiceImpl  {
    private final UserRepository userRepository;
    private final EmailValidator emailValidator;
    private final PasswordValidator passwordValidator;

    public void signUp(String email, String password) {

        emailValidator.validate(email);
        passwordValidator.validate(password);

        User user = User.builder()
                .email(email)
                .password(password)
                .build();

        userRepository.save(user);
    }
}
