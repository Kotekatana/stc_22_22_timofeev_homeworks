package com.kotekatana.services;

public interface UserService {
    void signUp(String email, String password);
}
