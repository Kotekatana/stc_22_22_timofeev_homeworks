package com.kotekatana.repositories;

import com.kotekatana.models.User;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class UserRepositoryFileImpl implements UserRepository{
    private final String filename;

    public UserRepositoryFileImpl(String filename) {
        this.filename = filename;
    }
    @Override
    public void save(User user){
        try(FileWriter fileWriter = new FileWriter(filename,true); BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)){
            String userToSave = user.getEmail() + ":" + user.getPassword();
            bufferedWriter.write(userToSave);
            bufferedWriter.newLine();
        }catch (IOException e ){
            throw new IllegalArgumentException(e);
        }
    }
}
