package com.kotekatana.repositories;

import com.kotekatana.models.User;

public interface UserRepository {
    public void save(User user);
}
