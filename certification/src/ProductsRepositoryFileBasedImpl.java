import java.io.*;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProductsRepositoryFileBasedImpl implements ProductsRepository {
    private final String fileName;
    public ProductsRepositoryFileBasedImpl(String fileName){
        this.fileName = fileName;
    }

    private static final Function<String,Product> stringToProductMapper = product ->{
        String[] parts = product.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String name = parts[1];
        Double price = Double.parseDouble(parts[2]);
        Integer quantity = Integer.parseInt(parts[3]);
        return new Product(id,name,price,quantity);
    };
    private static final Function<Product,String> productToStringMapper = product ->
         product.getId()+"|"+ product.getName()+"|"+product.getPrice()+"|"+ product.getQuantity();

    @Override
    public List<Product> findAll() {
        try {
            return new BufferedReader(new FileReader(fileName)).lines().map(stringToProductMapper).collect(Collectors.toList());
        }catch (IOException e ){
            System.err.println(e);
        }
        return null;
    }
    @Override
    public Product findById(Integer id) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))){
            return reader.lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getId() == id)
                    .findFirst().orElse(null);

        }catch (IOException e){
            throw new IllegalArgumentException(e);
        }
    }
    @Override
    public List<Product> findAllByTitleLike(String title) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))){
            return reader.lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getName().contains(title))
                    .collect(Collectors.toList());
        }catch (IOException e){
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Product product) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName));){
            List<String>data = reader.lines()
                    .map(stringToProductMapper)
                    .map(Product -> Product.getId() != product.getId() ? Product : product)
                    .map(productToStringMapper)
                    .collect(Collectors.toList());
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName,false));
            for (String prod : data) {
                writer.write(prod+"\n");
            }
            writer.close();
        }catch (IOException e){
            throw new IllegalArgumentException(e);
        }
    }
}
