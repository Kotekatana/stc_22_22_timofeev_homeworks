public class Main {
    public static void main(String[] args) {
        ProductsRepository productsRepository = new ProductsRepositoryFileBasedImpl("input.txt");
        Product butter = productsRepository.findById(3);
        butter.setPrice(199.9);
        butter.setQuantity(30);
        productsRepository.update(butter);


    }
}