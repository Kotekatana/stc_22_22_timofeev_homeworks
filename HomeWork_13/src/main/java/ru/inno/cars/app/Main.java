package ru.inno.cars.app;
import ru.inno.cars.models.Car;
import ru.inno.cars.repository.CarsRepository;
import ru.inno.cars.repository.CarsRepositoryJDBCImpl;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

import com.zaxxer.hikari.HikariDataSource;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
@Parameters(separators = "=")
public class Main {
    @Parameter(names = {"-action"})
    private  String action;

    public static void main(String[] args) {

        Main main = new Main();
        JCommander.newBuilder().addObject(main).build().parse(args);


        Properties dbProperties = new Properties();
        try{
            dbProperties.load(new BufferedReader(new InputStreamReader(Main.class.getResourceAsStream("/db.properties"))));
        }catch (IOException e){
            throw new IllegalArgumentException(e);
        }
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setPassword(dbProperties.getProperty("db.password"));
        dataSource.setUsername( dbProperties.getProperty("db.username"));
        dataSource.setJdbcUrl( dbProperties.getProperty("db.url"));
        dataSource.setMaximumPoolSize(Integer.parseInt(dbProperties.getProperty("db.hikari.maxPoolSize")));
        CarsRepository carsRepository = new CarsRepositoryJDBCImpl(dataSource);

        Scanner scanner = new Scanner(System.in);
        if(main.action.equals("read")){
            List<Car> cars = carsRepository.findAll();
            for(Car car:cars){
                System.out.println(car);
            }
        } else if (main.action.equals("write")) {
            while (true){
                System.out.println("Enter car model");
                String car_model = scanner.nextLine();
                System.out.println("Enter car color");
                String car_color = scanner.nextLine();
                System.out.println("Enter car number");
                String car_number = scanner.nextLine();
                System.out.println("Enter car driver id");
                String user_id = scanner.nextLine();
                Car car = Car.builder()
                        .model(car_model)
                        .color(car_color)
                        .car_number(car_number)
                        .user_id(Long.parseLong(String.valueOf(user_id)))
                        .build();
                carsRepository.save(car);
                System.out.println(car);
                if(scanner.nextInt() == -1){
                    break;
                }
            }
        }
    }
}