package ru.inno.cars.repository;

import ru.inno.cars.models.Car;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class CarsRepositoryJDBCImpl implements CarsRepository {
    private DataSource dataSource;
    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from cars";
    //language=SQL
    private static final String SQL_INSERT = "insert into cars(model, color, car_number, user_id) values (?,?,?,?)";
    public CarsRepositoryJDBCImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    private static final Function<ResultSet,Car> carRowMapper = row -> {
        try {
            return Car.builder()
                    .id(row.getLong("id"))
                    .model(row.getString("model"))
                    .car_number(row.getString("car_number"))
                    .color(row.getString("color"))
                    .user_id(row.getLong("user_id"))
                    .build();
        }catch (SQLException e ){
            throw new IllegalStateException(e);
        }

    };

    @Override
    public List<Car> findAll() {
        List<Car> cars = new ArrayList<>();
        try(Connection connection = dataSource.getConnection();Statement statement = connection.createStatement();){
            try(ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL)){
                while (resultSet.next()){
                    Car car = carRowMapper.apply(resultSet);
                    cars.add(car);
                }
            }
        }catch (SQLException e){
            throw new IllegalStateException(e);
        }
        return cars;
    }

    @Override
    public void save(Car car) {
        try(Connection connection = dataSource.getConnection();PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)){
            preparedStatement.setString(1, car.getModel());
            preparedStatement.setString(2, car.getColor());
            preparedStatement.setString(3, car.getCar_number());
            preparedStatement.setLong(4, car.getUser_id());

            int affectedRows = preparedStatement.executeUpdate();
            if(affectedRows !=1){
                throw new SQLException("Can not insert car");
            }
            try (ResultSet generatedId = preparedStatement.getGeneratedKeys()){
                if(generatedId.next()){
                    car.setId(generatedId.getLong("id"));
                }else{
                    throw new SQLException("Can not obtain generated Id");
                }
            }
        }catch (SQLException e){
            throw new IllegalStateException(e);
        }
    }
}
